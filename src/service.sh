#!/bin/bash

source /opt/pi_power_monitor/settings.conf

function send_email() {
	echo "$2" | sendemail -l email.log \
		-f "$smtp_address" \
		-u "$1" \
		-t "$recipient_email" \
		-s "smtp.gmail.com:587" \
		-xu "$smtp_address" \
		-xp "$smtp_password" \
		-o tls=yes
}

function check_power() {
	lsusb | grep "$usb_device" >/dev/null 2>&1
	return $?
}

was_down_last_cycle=0

# Run all the time
while :; do
	if check_power; then
		if [ "$was_down_last_cycle" == 1 ]; then
			# The power is back on.
            echo "$(date) - Power restored." >> "$log_file"
			was_down_last_cycle=0
			for i in $(seq 1 "$up_spam_amount")
			do
				send_email "Power restored at $location_name" "$(date)"
				sleep "$up_spam_time"
				check_power || break
			done
		fi
	else
		if [ "$was_down_last_cycle" == 0 ]; then
			# The power is down.
            echo "$(date) - Power down." >> "$log_file"
			for i in $(seq 1 "$down_spam_amount")
			do
				send_email "Power down at $location_name" "$(date)"
				sleep "$down_spam_time"
				check_power && break
			done
		fi
		was_down_last_cycle=1
	fi
	sleep "$check_interval"
done