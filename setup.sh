#!/bin/bash

install_location="/opt/pi_power_monitor/"

if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root."
    exit 1
fi

# Install required software
apt update
apt upgrade -y
apt autoremove -y
apt install -y sendemail libio-socket-ssl-perl libnet-ssleay-perl

# Copy script to Pi
mkdir "$install_location"
cp ./src/service.sh "$install_location/service.sh"
chmod +x "$install_location/service.sh"

# Get values from user
read -p "Enter GMail address to send from: " smtp_address
read -p "Enter GMail password for that account: " smtp_password
read -p "Enter email address to send to: " recipient_email
read -p "Enter the location name that will be in the email: " location_name

# Discover the hub's ID
clear
echo "Alright, unplug the hub if you plugged it in."
echo "Press [Y] when the hub is unplugged."
read -t 0.1
read -n1 -p "> "
echo "Just wait a monent..."
sleep 5
lsusb > /tmp/powerdetectlsusb1
clear
echo "Alright, now plug it in."
echo "Press [Y] when it is plugged in."
read -t 0.1
read -n1 -p "> "
echo "Just wait a moment..."
for i in {1..10}
do
    lsusb > /tmp/powerdetectlsusb2
    hubls="$(diff -u /tmp/powerdetectlsusb1 /tmp/powerdetectlsusb2 | grep -E '^\+' | tail -n1 -)"
    [ $( wc -w <<<"$hubls" ) -gt 0 ] && break
    sleep 2
done
clear

echo "Is this the hub?"
echo "$hubls"
read -t 0.1
read -n1 -p "[Y/n]"
if [ "$REPLY" = "Y" ] || [ "$REPLY" = "y" ] || [ "$REPLY" = "" ]
then
    set -- $hubls
    usb_device="$6"
else
    echo "If that is not the hub, then I cannot tell what is."
    while :;
    do
        clear
        echo "You will need to enter the ID manually."
        echo -e "\n\nHere is the output of lsusb:"
        lsusb
        echo -e "\n\nThe ID is two sets of 4 hex digits divided by a colon."
        echo "Enter the USB ID of the hub."
        read -p "> " id_input
        if grep "$id_input" <<<"$(lsusb)"
        then
            break
        else
            echo "ID does not exsist.  Try again."
        fi
    done


fi
clear
echo "Alright, the device ID is $usb_device, in case you care."

# Write all that data to the config file
echo "smtp_address=\"$smtp_address\"" > "$install_location/settings.conf"
echo "smtp_password=\"$smtp_password\"" >> "$install_location/settings.conf"
echo "recipient_email=\"$recipient_email\"" >> "$install_location/settings.conf"
echo "usb_device=\"$usb_device\"" >> "$install_location/settings.conf"
echo "location_name=\"$location_name\"" >> "$install_location/settings.conf"

echo "down_spam_time=15" >> "$install_location/settings.conf"
echo "down_spam_amount=10" >> "$install_location/settings.conf"
echo "up_spam_time=15" >> "$install_location/settings.conf"
echo "up_spam_amount=3" >> "$install_location/settings.conf"
echo "check_interval=1" >> "$install_location/settings.conf"

echo "log_file=\"$install_location/power.log\"" >> "$install_location/settings.conf"

# Autostart script
cp ./src/pi_power_monitor.service /etc/systemd/system/pi_power_monitor.service
systemctl daemon-reload
systemctl start pi_power_monitor
systemctl enable pi_power_monitor

echo "Setup Complete."